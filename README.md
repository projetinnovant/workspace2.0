### Repo pour le BluePill Project ###



## Some explanations : ##

* This is a repo for a project done as part of our course ([@emucignato](https://twitter.com/emucignato) and [@Drenghel](https://twitter.com/Drenghel) ) for our school [Licence Pro "Développeur Application Web et Innovations Numériques"](http://www.iut.u-bordeaux.fr/info/)  


## What is it ? ##

It was about using a [SP360 - Kodak PIXPRO](http://kodakpixpro.com/Americas/cameras/actioncamera/sp360.php) and an Oculus Rift DK2 together, and for that we created a portal themed scene with a hub room leading to 3 projections rooms, one for each "mode":

* Watching a video taken in front of a surfing person in the Web Surf cafe in Bordeaux  
* Watching a web downloaded video ( currently a 360 movie at the Mont Blanc )  
* And finally, watching a stream in LIVE from the kodak camera, filmed in 360  

## How does it work: ##

* UnityProjects / Blue Pill Project V2  is the folder of our latest version of the demo / game  
* NodeImgServer  is the folder of the the node web server and node slicing script for the video, there are bat files to launch them (but adapted to my computer )  

### Warning ###

* **Don't expect the live stream to work unless you have the same camera, or a camera streaming at this IP : http://172.16.0.254:9176 and with a MJEPG stream**  
* **There is no built version of the game, for our presentation we used the preview, so it may lack optimization but ...**  
* ... Here's a hosted version of the last project folder on mega for convenience ( it's a bit heavy due to our video asset folder :) )  : [HERE !](
https://mega.nz/#!NBIjGSCK!vfsbiVyYqg2rcjEA3tVtcLtH1ocIBrZgdd7rinPFxwI)

### Link to our slide presentation ###

[Slides](http://slides.com/estellemucignato/bluepillproject)