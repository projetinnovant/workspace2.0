using UnityEngine;
using System.Collections;

public class videoCtrl : MonoBehaviour {

	public GameObject Tunnel;
	public GameObject walkway;
	public GameObject contour;
	public GameObject button;
	public GameObject cube;
	public string buttonToPress;

	void Update () {
		if (Input.GetButton(buttonToPress)) {

			Renderer r = GetComponent<Renderer>();
			MovieTexture movie = (MovieTexture)r.material.mainTexture;
			movie.loop = true;

			if (movie.isPlaying) {
				movie.Pause();
				Tunnel.GetComponent<MeshRenderer>().enabled = true;
				contour.GetComponent<MeshRenderer>().enabled = true;
				button.GetComponent<MeshRenderer>().enabled = true;
				walkway.GetComponent<MeshRenderer>().enabled = true;
					cube.GetComponent<MeshRenderer>().enabled = true;

			}
			else {
				movie.Play();
				Tunnel.GetComponent<MeshRenderer>().enabled = false;
				contour.GetComponent<MeshRenderer>().enabled = false;
				button.GetComponent<MeshRenderer>().enabled = false;
				walkway.GetComponent<MeshRenderer>().enabled = false;
				cube.GetComponent<MeshRenderer>().enabled = false;
			}
		}
	}
}
