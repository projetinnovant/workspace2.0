﻿using UnityEngine;
using System.Collections;

public class TriggerButtonController : MonoBehaviour {
	public bool close = false;
	public AudioSource audio1;
	public AudioSource audio2;

	void Update(){
		if (!close) {
			if (audio1 != null){
				audio1.Play ();
			}
			if(audio2 != null){
				audio2.PlayDelayed (15);
			}
		}
	}

	void OnTriggerEnter(Collider obj){
		if (obj.transform.name == "Player")
			close = true;
	}

	void OnTriggerExit(Collider obj){
		close = false;
	}


}
