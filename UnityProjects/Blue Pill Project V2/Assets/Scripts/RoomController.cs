﻿using UnityEngine;
using System.Collections;

public class RoomController : MonoBehaviour {

	public GameObject player;
	public AudioSource start;
	public AudioSource Room1_01; 
	// Use this for initialization
	void Start () {
		start.Play ();
		Room1_01.PlayDelayed(15);
	}
	
	// Update is called once per frame
	void Update () {
		if (start.isPlaying) {
			player.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeAll;
			player.transform.position = new Vector3(0, 0.196076F, 2.17F);
		} else {
			player.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.None;

		}
		if(Input.GetKeyDown(KeyCode.Q))
		{
			Application.Quit();    
		}
	}
}
