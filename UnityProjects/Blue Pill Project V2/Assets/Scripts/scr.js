#pragma strict

// Continuously get the latest webcam shot from outside "Friday's" in Times Square
// and DXT compress them at runtime
var url = "http://localhost:8080/latest.jpg";
var www;

var emission = 1.0;

function Start () {
	// Create a texture in DXT1 format
	GetComponent.<Renderer>().material.mainTexture = new Texture2D(4, 4, TextureFormat.DXT1, false);
	GetComponent.<Renderer>().material.EnableKeyword("_EMISSION");

}

function Update () {
	loadTexture();
}


function loadTexture(){
	// assign the downloaded image to the main texture of the object
	var www = new WWW(url);
	// wait until the download is done
	yield www;

	www.LoadImageIntoTexture(GetComponent.<Renderer>().material.mainTexture);
	GetComponent.<Renderer>().material.SetTextureScale("_MainTex", new Vector2(-1,1));
	// I USE THIS PART DO TO THAT WITH AN EMMISSIVE COLOR
	var baseColor = Color.white;
	var finalColor = baseColor * Mathf.LinearToGammaSpace (emission);
	GetComponent.<Renderer>().material.SetColor("_EmissionColor", finalColor);

	GetComponent.<Renderer>().material.SetTexture("_EmissionMap", GetComponent.<Renderer>().material.mainTexture);
	GetComponent.<Renderer>().material.SetTextureScale("_EmissionMap", new Vector2(-1,1));

}
