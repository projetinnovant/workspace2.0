﻿using UnityEngine;
using System.Collections;

public class TriggerController : MonoBehaviour {
	public bool opening = false;
	public bool closing = false;
	public GameObject wall;
	
	// Use this for initialization
	void Start () {
		string tag = wall.transform.tag;
	}
	
	// Update is called once per frame
	void Update () {
		if (opening) {
			wall.GetComponent<Animation>()["OpenDoor"+tag].speed = 1;
			wall.GetComponent<Animation>().Play("OpenDoor"+tag);
			if (wall.GetComponent<AudioSource>() != null)
				wall.GetComponent<AudioSource>().Play();
			if (GetComponent<AudioSource>() != null)
				GetComponent<AudioSource>().Play();
			
			opening = false;
		}
		if (closing) {
			wall.GetComponent<Animation>()["OpenDoor"+tag].speed = -1;
			wall.GetComponent<Animation> ()["OpenDoor"+tag].time = wall.GetComponent<Animation> () ["OpenDoor"+tag].length;
			wall.GetComponent<Animation>().Play("OpenDoor"+tag);
			closing = false;
		}
	}

	void OnTriggerEnter(Collider obj){
		if (obj.transform.name == "Player") {
			opening = true;
			closing = false;
		}
	}

	void OnTriggerExit(Collider obj){
		if (obj.transform.name == "Player") {
			opening = false;
			closing = true;
		}
	}
}
