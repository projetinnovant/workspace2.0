﻿using UnityEngine;
using System.Collections;

public class TriggerController : MonoBehaviour {
	public bool opening = false;
	public bool closing = false;
	public GameObject wall;
	public GameObject player;
	// Use this for initialization
	void Start () {
		//spotlight.GetComponent<Animation> ().Play ();
	}
	
	// Update is called once per frame
	void Update () {
		if (opening) {
			//doorLeft.GetComponent<Animation> ().Play ("Open"+triggerName+"Left");
			//doorRight.GetComponent<Animation> ().Play ("Open"+triggerName+"Right");
			wall.GetComponent<Animation>()["OpenDoor"].speed = 1;
			wall.GetComponent<Animation>().Play("OpenDoor");
			if (GetComponent<AudioSource>() != null)
				GetComponent<AudioSource>().Play();
			
			opening = false;
		}
		if (closing) {
			//doorLeft.GetComponent<Animation> ().Play ("Close"+triggerName+"Left");
			//doorRight.GetComponent<Animation> ().Play ("Close"+triggerName+"Right");
			wall.GetComponent<Animation>()["OpenDoor"].speed = -1;
			wall.GetComponent<Animation> ()["OpenDoor"].time = wall.GetComponent<Animation> () ["OpenDoor"].length;
			wall.GetComponent<Animation>().Play("OpenDoor");
			if (GetComponent<AudioSource>() != null)
				GetComponent<AudioSource>().Play();
			closing = false;
		}
	}

	void OnTriggerEnter(Collider obj){
		if (obj.transform.name == "Player") {
			opening = true;
			closing = false;
		}
	}

	void OnTriggerExit(Collider obj){
		if (obj.transform.name == "Player") {
			opening = false;
			closing = true;
		}
	}
}
